# ConcreteCMS + Docker + Saltwater

This repository is intended to be used by developers at Saltwater 
to make it easy to work with an existing concrete5 (>= v8.5.1) 
client site locally.

### Install

- Confirm [docker](https://www.docker.com/) is installed on your machine
- Download this repository and copy the files (except this readme) into the existing client site project in the topmost directory.
- Copy `.env.example` to `.env` and edit the file to change config values
- From the topmost directory, run this command from your terminal:`docker-compose up`
- Visit: http://localhost

### After installation
_Note that the below docker generated files are created in a parent directory, so that must be writable!_

- a ../docker/ folder will be created that contains:
  - Apache config files `../docker/PROJECT_NAME/etc/apache2`
  - MySQL persisted data `../docker/PROJECT_NAME/var/lib/mysql`
  
- phpmyadmin is available for easy DB access & simple actions 
  - Visit: http://localhost:8080

- Or, you can connect programmatically to the DB from the host machine on port 3306

### Future improvements

- [ ] Fix SSL Issues
